package cn.nxcoder.blog.model;

import com.github.pagehelper.PageInfo;

import java.util.List;

public class Page<T> extends PageInfo<T> {

    private List<BlogVo> blogList;

    public Page(List<T> list) {
        super(list);
    }

    public List<BlogVo> getBlogList() {
        return blogList;
    }

    public void setBlogList(List<BlogVo> blogList) {
        this.blogList = blogList;
    }


}
