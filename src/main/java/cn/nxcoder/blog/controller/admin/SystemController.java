package cn.nxcoder.blog.controller.admin;

import cn.nxcoder.blog.entity.Blog;
import cn.nxcoder.blog.entity.Tag;
import cn.nxcoder.blog.entity.Type;
import cn.nxcoder.blog.service.BlogService;
import cn.nxcoder.blog.service.TagService;
import cn.nxcoder.blog.service.TypeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 关于我
 *
 * @author shengwu ni
 * @date 2018/05/12 22:31
 */
@Controller
@RequestMapping("/admin")
public class SystemController {

    @Resource
    private BlogService blogService;
    @Resource
    private TypeService typeService;
    @Resource
    private TagService tagService;

    @GetMapping("/refresh")
    public String refresh(HttpServletRequest request) {
        ServletContext application = request.getServletContext();

        // 查询出6条博客类型数据（按照包含博客数量多少降序）
        List<Type> typeList = typeService.findAllTypes();
        // 查询出10条博客标签数据（按照包含博客数量多少降序）
        List<Tag> tagList = tagService.findAllTags();
        // 查询出访问量最多的5条记录
        List<Blog> topView = blogService.findTop5Views();

        application.setAttribute("types", typeList);
        application.setAttribute("tags", tagList);
        application.setAttribute("views", topView);
        application.setAttribute("total", blogService.findAll().size());

        return "admin/refresh";
    }
}
