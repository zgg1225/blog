package cn.nxcoder.blog.controller.admin;

import cn.nxcoder.blog.entity.Blog;
import cn.nxcoder.blog.entity.Type;
import cn.nxcoder.blog.service.BlogService;
import cn.nxcoder.blog.service.TypeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * 博客类别Controller
 * @author shengwu ni
 * @date 2018/05/05 15:45
 */
@Controller
@RequestMapping("/admin")
public class TypeController {

    @Resource
    private TypeService typeService;
    @Resource
    private BlogService blogService;

    /**
     * 分页查询博客类型
     * @param page 第几页
     * @param model 保存查到的记录
     * @return String
     */
    @GetMapping("/types")
    public String typeList(@RequestParam(defaultValue = "1") Integer page, Model model) {
        // 每页显示5条记录
        PageHelper.startPage(page, 5);
        List<Type> list = typeService.findAll();
        PageInfo<Type> pageInfo = new PageInfo<>(list);
        model.addAttribute("page", pageInfo);
        return "admin/types";
    }

    /**
     * 新增类型跳转接口
     * @return String
     */
    @GetMapping("/types/input")
    public String input(Model model) {
        model.addAttribute("type", new Type());
        return "admin/types-input";
    }

    /**
     * 新增或更新博客类别
     * @param type 博客类别实体，需要校验的
     * @param result 校验的结果
     * @param attributes 用于重定向带数据到前端
     * @return String
     */
    @PostMapping("/addOrUpdateTypes")
    public String post(@Valid Type type, BindingResult result, RedirectAttributes attributes) {

        // 如果type中的name校验为空（在实体中有注解校验），会重定向到types-input页，并且会把错误消息带过去
        if (result.hasErrors()) {
            return "/admin/types-input";
        }

        // 业务上校验，新增的分类名称不能和已有的重复
        Type typeByName = typeService.findBy("name", type.getName());
        if (typeByName != null) {
            result.rejectValue("name", "nameError", "该类别名称已经存在，不能重复添加");
            return "/admin/types-input";
        }

        Integer r;
        if (type.getId() == null) {
            r = typeService.save(type);
        } else {
            r = typeService.update(type);
        }

        // 对插入数据库的操作进行校验，验证是否正常插入或更新了
        if (r == null) {
            attributes.addFlashAttribute("message", "操作失败");
        } else {
            attributes.addFlashAttribute("message", "操作成功");
        }
        return "redirect:/admin/types";
    }

    /**
     * 编辑博客类别
     * @param id 类别id
     * @param model 将查询到的类别实体带到前端
     * @return String
     */
    @GetMapping("/types/{id}/input")
    public String editInput(@PathVariable Long id, Model model) {
        model.addAttribute("type", typeService.findById(id));
        return "admin/types-input";
    }

    /**
     * 删除博客类别
     * @param id 类别id
     * @param attributes 用于重定向带数据到前端
     * @return
     */
    @GetMapping("/types/{id}/delete")
    public String delete(@PathVariable Long id, RedirectAttributes attributes) {

        // 首先查询该类别下有没有博客
        List<Blog> blogList = blogService.findByTypeId(id);
        if (blogList != null && blogList.size() > 0) {
            attributes.addFlashAttribute("message", "操作失败，该类别下有博客，不能删除");
            return "redirect:/admin/types";
        }
        Integer res = typeService.deleteById(id);
        if (res != null) {
            attributes.addFlashAttribute("message", "操作成功");
        } else {
            attributes.addFlashAttribute("message", "操作失败");
        }
        return "redirect:/admin/types";
    }
}
