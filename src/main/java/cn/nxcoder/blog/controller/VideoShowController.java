package cn.nxcoder.blog.controller;

import cn.nxcoder.blog.entity.Video;
import cn.nxcoder.blog.exception.NonBlogException;
import cn.nxcoder.blog.service.VideoService;
import cn.nxcoder.blog.util.MarkdownUtils;
import com.vdurmont.emoji.EmojiParser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.List;

/**
 * 视频课程页面
 * @author shengwu ni
 * @date 2018/06/03 16:40
 */
@Controller
public class VideoShowController {

    @Resource
    private VideoService videoService;

    @GetMapping("/videos")
    public String vedios(Model model) {

        List<Video> videoList = videoService.findAll();
        model.addAttribute("videos", videoList);
        model.addAttribute("title", "视频教程列表");
        return "videos";
    }

    @GetMapping("/videos/{id}")
    public String getVideo(@PathVariable Long id, Model model) {

        Video video = videoService.findById(id);
        if (video == null) {
            throw new NonBlogException("该教程不存在，请联系本章站长。");
        }
        // 更新一下访问量
        video.setViews(video.getViews() + 1);
        videoService.update(video);

        // 先将emoji表情还原
        video.setContent(EmojiParser.parseToUnicode(video.getContent()));
        // 将markdown转成html
        video.setContent(MarkdownUtils.markdownToHtmlExtensions(video.getContent()));
        model.addAttribute("video", video);
        model.addAttribute("title", video.getName() + "视频详情");
        return "video";
    }

}
