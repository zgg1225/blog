package cn.nxcoder.blog.controller;

import cn.nxcoder.blog.dao.TagMapper;
import cn.nxcoder.blog.entity.Blog;
import cn.nxcoder.blog.entity.Tag;
import cn.nxcoder.blog.entity.Type;
import cn.nxcoder.blog.entity.User;
import cn.nxcoder.blog.model.BlogVo;
import cn.nxcoder.blog.model.Page;
import cn.nxcoder.blog.service.BlogService;
import cn.nxcoder.blog.service.TagService;
import cn.nxcoder.blog.service.TypeService;
import cn.nxcoder.blog.service.UserService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 博客标签页面
 * @author shengwu ni
 * @date 2018/05/12 16:45
 */
@Controller
public class TagShowController {

    @Resource
    private TypeService typeService;
    @Resource
    private BlogService blogService;
    @Resource
    private UserService userService;
    @Resource
    private TagService tagService;

    @GetMapping("/tags/{id}")
    public String types(@RequestParam(defaultValue = "1") Integer page,
                        @PathVariable Long id, Model model) {

        // 查询出博客标签数据（按照包含博客数量多少降序）
        List<Tag> tagList = tagService.findAllTags();
        if (tagList != null) {
            if (id == -1) {
                id =  tagList.get(0).getId();
            }
            model.addAttribute("tags", tagList);
            // 每页显示8条记录
            PageHelper.startPage(page, 8);
            List<Blog> blogList = blogService.findAllByTagIdAndCreateTimeDesc(id);
            Page<Blog> pageInfo = new Page<>(blogList);
            List<BlogVo> blogInfoList = getBlogInfoList(blogList);
            pageInfo.setBlogList(blogInfoList);
            model.addAttribute("page", pageInfo);
            model.addAttribute("activeTagId", id);
            model.addAttribute("title", "博客标签");
        }

        return "tags";
    }

    private List<BlogVo> getBlogInfoList(List<Blog> list) {

        List<BlogVo> blogInfoList = new ArrayList<>();

        // 封装信息
        if (list != null && list.size() > 0) {
            list.forEach((b) -> {
                BlogVo blogVo = new BlogVo();
                BeanUtils.copyProperties(b, blogVo);
                blogVo.setUpdate(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(b.getUpdateTime()));
                blogVo.setCreate(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(b.getCreateTime()));

                // 封装type信息
                Long typeid = b.getTypeid();
                Type type = typeService.findById(typeid);
                blogVo.setType(type);

                // 封装user信息
                Long userid = b.getUserid();
                User user = userService.findById(userid);
                blogVo.setUser(user);

                // 封装tag信息
                List<Tag> tags = new ArrayList<>();
                String tagids = b.getTagid();
                String[] tagArr = tagids.split(",");
                for (String tagid : tagArr) {
                    Tag tag = tagService.findById(Long.valueOf(tagid));
                    tags.add(tag);
                }
                blogVo.setTags(tags);

                blogInfoList.add(blogVo);
            });
        }
        return blogInfoList;
    }
}
