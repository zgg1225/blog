package cn.nxcoder.blog.handler;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登陆过滤器，凡是地址中有admin的都要过滤，防止未登录就能访问博客后台
 * @author shengwu ni
 * @date 2018/05/05 12:57
 */
public class LoginInterceptorHandler extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (request.getSession().getAttribute("user") == null) {
            // 如果session中没有用户信息，重定向到登陆接口
            response.sendRedirect("/admin");
            return false;
        } else {
            return true;
        }
    }
}
