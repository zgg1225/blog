package cn.nxcoder.blog.service;


import cn.nxcoder.blog.core.Service;
import cn.nxcoder.blog.entity.Comment;

import java.util.List;


public interface CommentService extends Service<Comment> {

    List<Comment> listCommentByBlogId(Long blogId);

    Integer saveComment(Comment comment);

}
