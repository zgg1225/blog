package cn.nxcoder.blog.service;


import cn.nxcoder.blog.core.Service;
import cn.nxcoder.blog.entity.Blog;

import java.util.List;
import java.util.Map;


public interface BlogService extends Service<Blog> {

    List<Blog> findByTypeId(Long id);

    List<Blog> findAllByCreateTimeDesc();

    List<Blog> findTop5Views();

    List<Blog> findAllByTypeIdAndCreateTimeDesc(Long id);

    List<Blog> findAllByTagIdAndCreateTimeDesc(Long id);

    Map<String,List<Blog>> archiveBlogList();

    Integer blogCount();

    List<Blog> findAllByUpdateTimeDesc();

}
