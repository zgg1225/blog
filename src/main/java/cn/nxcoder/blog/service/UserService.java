package cn.nxcoder.blog.service;

import cn.nxcoder.blog.core.Service;
import cn.nxcoder.blog.entity.User;

public interface UserService extends Service<User> {

    User checkUser(String username, String password);
}
