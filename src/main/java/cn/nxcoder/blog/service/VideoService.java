package cn.nxcoder.blog.service;


import cn.nxcoder.blog.core.Service;
import cn.nxcoder.blog.entity.Video;


public interface VideoService extends Service<Video> {
}
