package cn.nxcoder.blog.service.impl;

import cn.nxcoder.blog.core.AbstractService;
import cn.nxcoder.blog.entity.Video;
import cn.nxcoder.blog.service.VideoService;
import org.springframework.stereotype.Service;

/**
 * 视频Service
 * @author shengwu ni
 * @date 2018/06/03 17:40
 */
@Service
public class VideoServiceImpl extends AbstractService<Video> implements VideoService {

}
