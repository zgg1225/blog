package cn.nxcoder.blog.service.impl;

import cn.nxcoder.blog.core.AbstractService;
import cn.nxcoder.blog.dao.BlogMapper;
import cn.nxcoder.blog.dao.TagMapper;
import cn.nxcoder.blog.entity.Blog;
import cn.nxcoder.blog.entity.Tag;
import cn.nxcoder.blog.service.BlogService;
import cn.nxcoder.blog.service.TagService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 博客Service
 * @author shengwu ni
 * @date 2018/05/05 21:50
 */
@Service
public class BlogServiceImpl extends AbstractService<Blog> implements BlogService {

    @Resource
    private BlogMapper blogMapper;

    @Override
    public List<Blog> findByTypeId(Long id) {
        return blogMapper.findByTypeId(id);
    }

    @Override
    public List<Blog> findAllByCreateTimeDesc() {
        return blogMapper.findAllByCreateTimeDesc();
    }

    @Override
    public List<Blog> findTop5Views() {
        List<Blog> blogs = blogMapper.findAllByViewsDesc();
        if (blogs != null && blogs.size() > 0) {
            if (blogs.size() <= 5) {
                blogs.sort((o1, o2) -> o2.getViews().compareTo(o1.getViews()));
                return blogs;
            } else {
                blogs.sort((o1, o2) -> o2.getViews().compareTo(o1.getViews()));
                return blogs.subList(0, 4);
            }
        }
        return null;
    }

    @Override
    public List<Blog> findAllByTypeIdAndCreateTimeDesc(Long id) {
        return blogMapper.findAllByTypeIdAndCreateTimeDesc(id);
    }

    @Override
    public List<Blog> findAllByTagIdAndCreateTimeDesc(Long id) {
        // 查出该标签下的所有博客
        Example example = new Example(Blog.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.orEqualTo("tagid", id + "");
        criteria.orLike("tagid", "%," + id + ",%");
        criteria.orLike("tagid", "%," + id + "%");
        criteria.orLike("tagid", "%" + id + ",%");
        List<Blog> blogList = blogMapper.selectByExample(example);
        // 对blogList按照create_time进行降序排列
        blogList.sort(((o1, o2) -> o2.getCreateTime().compareTo(o1.getCreateTime())));
        return blogList;
    }

    @Override
    public Map<String, List<Blog>> archiveBlogList() {
        List<String> years = blogMapper.findGroupYear();
        Map<String, List<Blog>> map = new HashMap<>();
        if (years!= null && years.size() > 0) {
            for (String year : years) {
                map.put(year, blogMapper.findByYear("%" + year + "%"));
            }
        }
        return map;
    }

    @Override
    public Integer blogCount() {
        return blogMapper.blogCount();
    }

    @Override
    public List<Blog> findAllByUpdateTimeDesc() {
        return blogMapper.findAllByUpdateTimeDesc();
    }
}
