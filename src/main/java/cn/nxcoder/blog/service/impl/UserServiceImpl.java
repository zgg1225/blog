package cn.nxcoder.blog.service.impl;

import cn.nxcoder.blog.core.AbstractService;
import cn.nxcoder.blog.dao.UserMapper;
import cn.nxcoder.blog.entity.User;
import cn.nxcoder.blog.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 用户Service
 * @author shengwu ni
 * @date 2018/05/05 1:10
 */
@Service
public class UserServiceImpl extends AbstractService<User> implements UserService {

    @Resource
    private UserMapper userMapper;
    @Override
    public User checkUser(String username, String password) {
        return userMapper.findUserByNameAndPass(username, password);
    }
}
