package cn.nxcoder.blog.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * 博客类型实体
 * @author shengwu ni
 * @date 2018/05/04 21:31
 */
@Table(name = "nx_type")
public class Type {

    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 类型名称
     */
    @NotBlank(message = "分类名称不能为空")
    private String name;

    /**
     * 该类别下博客数量，用于前端展示
     */
    @Transient
    private Integer blogNum;

    public Type() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBlogNum() {
        return blogNum;
    }

    public void setBlogNum(Integer blogNum) {
        this.blogNum = blogNum;
    }

    @Override
    public String toString() {
        return "Type{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

}
