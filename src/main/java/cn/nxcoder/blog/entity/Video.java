package cn.nxcoder.blog.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * 视频实体
 * @author shengwu ni
 * @date 2018/06/06 17:35
 */
@Table(name = "nx_video")
public class Video {

    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 视频名称
     */
    private String name;

    /**
     * 视频说明
     */
    private String content;

    /**
     * 视频图片
     */
    private String picture;

    /**
     * 视频点击量
     */
    private Integer views;

    /**
     * 视频时长
     */
    private Integer time;

    /**
     * 视频标签
     */
    private String tag;

    /**
     * 视频价格
     */
    private Double price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
