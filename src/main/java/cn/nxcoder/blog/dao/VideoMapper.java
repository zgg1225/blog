package cn.nxcoder.blog.dao;

import cn.nxcoder.blog.core.MyMapper;
import cn.nxcoder.blog.entity.Tag;
import cn.nxcoder.blog.entity.Video;
import org.apache.ibatis.annotations.Delete;

/**
 * 视频Mapper
 * @author shengwu ni
 * @date 2018/06/03 17：42
 */
public interface VideoMapper extends MyMapper<Video> {

}
