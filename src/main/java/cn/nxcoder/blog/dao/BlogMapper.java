package cn.nxcoder.blog.dao;

import cn.nxcoder.blog.core.MyMapper;
import cn.nxcoder.blog.entity.Blog;
import cn.nxcoder.blog.entity.Type;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 博客Mapper
 * @author shengwu ni
 * @date 2018/05/05 15:32
 */
public interface BlogMapper extends MyMapper<Blog> {

    @Select("select * from nx_blog where published = 1 order by create_time desc")
    @ResultMap("BaseResultMap")
    List<Blog> findAllByCreateTimeDesc();

    @Select("select count(*) from nx_blog where typeid = #{id}")
    Integer findCount(Long id);

    @Select("select * from nx_blog where typeid = #{id}")
    @ResultMap("BaseResultMap")
    List<Blog> findByTypeId(Long id);

    @Select("select * from nx_blog order by views desc")
    @ResultMap("BaseResultMap")
    List<Blog> findAllByViewsDesc();

    @Select("select * from nx_blog where typeid = #{id} order by create_time desc")
    @ResultMap("BaseResultMap")
    List<Blog> findAllByTypeIdAndCreateTimeDesc(Long id);

    @Select("SELECT DATE_FORMAT(b.create_time,'%Y') as year FROM nx_blog b GROUP BY year ORDER BY year DESC")
    List<String> findGroupYear();

    @Select("select * from nx_blog where create_time like #{year} ORDER BY create_time DESC")
    List<Blog> findByYear(String year);

    @Select("select count(*) from nx_blog")
    Integer blogCount();

    @Select("select * from nx_blog where published = 1 order by update_time desc")
    @ResultMap("BaseResultMap")
    List<Blog> findAllByUpdateTimeDesc();
}
