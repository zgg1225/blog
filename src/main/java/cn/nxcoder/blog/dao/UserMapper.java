package cn.nxcoder.blog.dao;

import cn.nxcoder.blog.core.MyMapper;
import cn.nxcoder.blog.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 用户Mapper
 * @author shengwu ni
 * @date 2018/05/05 01:41
 */
public interface UserMapper extends MyMapper<User> {

    /**
     * 根据用户名和密码查询用户
     * @param username 用户名
     * @param password 密码
     * @return User 用户实体
     */
    @Select("select * from nx_user where username = #{user} and password = #{pass}")
    User findUserByNameAndPass(@Param(value = "user") String username, @Param(value = "pass") String password);
}
