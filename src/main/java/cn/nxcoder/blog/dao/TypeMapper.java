package cn.nxcoder.blog.dao;

import cn.nxcoder.blog.core.MyMapper;
import cn.nxcoder.blog.entity.Type;
import org.apache.ibatis.annotations.Delete;

/**
 * 博客类型Mapper
 * @author shengwu ni
 * @date 2018/05/05 15:32
 */
public interface TypeMapper extends MyMapper<Type> {

    /**
     * 根据id删除博客类型
     * @param id id
     * @return Integer
     */
    @Delete("delete from nx_type where id = #{id}")
    Integer deleteById(Long id);
}
